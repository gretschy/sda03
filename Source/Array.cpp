//
//  Array.cpp
//  CommandLineTool
//
//  Created by Tom on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "Array.hpp"
#include <iostream>

using namespace std;

Array::Array()     //constructor
{
    std::cout << "Array created\n";
    arrayPtr = new float[arraySize];
}

Array::~Array()   //destructor
{
    std::cout << "Array destroyed\n";
    delete [] arrayPtr;         //release memory
    
}

void Array::add(float value)
{
    arraySize++;
    float * tempPtr = new float[arraySize];
    
    for (int i = 0; i <= arraySize; i++)
    {
        tempPtr[i] = arrayPtr[i];
    }
    
    delete [] arrayPtr;
    
    tempPtr[arraySize - 1] = value;
    
    arrayPtr = tempPtr;   
    
}


float Array::get(int index) const
{
    return arrayPtr [index];
}


int Array::size() const
{
    int numberOfItems = arraySize;
    
    return numberOfItems;
}
