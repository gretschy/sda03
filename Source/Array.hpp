//
//  Array.hpp
//  CommandLineTool
//
//  Created by Tom on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>
#include <iostream>

class Array
{
public:
    
    Array();            /**constructor*/
    
    ~Array();           /**destructor*/
    
    void add(float ItemValue);          /**mutator*/
    float get(int index) const;          /**accessor*/
    int size() const;                  /**accessor*/
    
private:
    int arraySize;
    float *arrayPtr = nullptr;
    
    
};


#endif /* Array_hpp */
