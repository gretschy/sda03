//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Tom on 09/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"
#include <iostream>

using namespace std;

LinkedList::LinkedList()
{
    cout << "class LinkedList created\n";
    head = nullptr;
    
}
LinkedList::~LinkedList()
{
    clear();
    cout << "class LinkedList destroyed\n";
}

void LinkedList::add(float addData)
{
   
    Node* n = new Node;  //make a new node called 'n' and pointer to that new node
    n->next = nullptr; //make it point to null, and be the last node in the list
    n->data = addData; //pass the value of addData into the data variable inside the node
    
    Node* current;
    
    if (head != nullptr) //if a list has been created, make the current pointer point to the start of the list
    {
        current = head;
        
        while (current->next != nullptr) //while it's not at the end of the list, advance the current pointer to the next node in the list
        {
            current = current->next;
        }
        
        current->next = n; //when it gets to the end of the list, exit the while loop and say thay the next element points to the new node that was created earlier
        
    }
    else  //if there is no list already, make 'n' the start of the list
    {
        head = n;
    }
}

float LinkedList::get(int requestedIndex) const
{
    if (head == nullptr)
        return 0.f;

    Node* current = head;
    int currentIndex = 0;
    
    while (current != nullptr && currentIndex < requestedIndex)
    {
        current = current->next;
        currentIndex++;
    }
    
    return current->data;
}


int LinkedList::size() const
{
    Node* current = head;

    int sizeOfList = 0;
    
    while (current != nullptr)
    {
        current = current->next;
        sizeOfList++;
    }
    
    return sizeOfList;
}

void LinkedList::clear() 
{
    Node *deletePtr = head;
    
    while (deletePtr != nullptr)    //traverse the list until it reaches the null pointer
    {
        head = head->next;      //delete one by one from the head end
        delete deletePtr;
        deletePtr = head;       //update the head as it goes along
    }
    head = nullptr;             //reset the head when it exits the while loop
}
