//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "Array.hpp"
#include "LinkedList.hpp"

using namespace std;

bool testArray();

int main ()
{
    
    LinkedList list;
    
        if (testArray() == true)
        {
            std::cout << "array tests passed\n";
        }
        else
        {
            std::cout << "array tests failed\n";
        }
    
    return 0;
}

bool testArray()
{
    LinkedList array;

    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof(testArray) / sizeof(testArray[0]);

    if (array.size() !=0)
    {
        std::cout << "size is wrong \n";
        return false;
    }

    for (int i =0; i < testArraySize;  i++)
    {
        array.add(testArray[i]);

        if (array.size() != i+1)
        {
            std::cout << "size is wrong \n";
            return false;
        }

        if (array.get(i) != testArray[i])
        {
            std::cout << "value at index: " << i << "recalled incorrectly\n";
            return false;
        }

    }
    return true;
}



